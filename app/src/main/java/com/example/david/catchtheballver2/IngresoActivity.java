package com.example.david.catchtheballver2;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class IngresoActivity extends AppCompatActivity {

    Button jugar, salir, comojugar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingreso);


    }

    public void Jugar(View view){

        Intent intent = new Intent( IngresoActivity.this, MainActivity.class );
        startActivity(intent);
    }


    public void Salir(View view){

        finish();
    }


    public void comoJugar(View view){

        Intent intent = new Intent( IngresoActivity.this, InstruccionesActivity.class );
        startActivity(intent);
    }

}
